const { Sequelize } = require('sequelize');
const connection = require('../config/config');

// Tables

const Games = connection.define('Games', {

    title: {
        type: Sequelize.STRING
    },
    year: {
        type: Sequelize.INTEGER
    },
    price: {
        type: Sequelize.DOUBLE
    }

});

//Games.sync({force: true});

module.exports = Games;

