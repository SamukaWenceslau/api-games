const express = require('express');
const router = express.Router();

const GamesController = require('../controller/GamesController');

router
   .get("/games", GamesController.index)
   .get("/game/:id", GamesController.show)
   .post("/game", GamesController.create)
   .put("/game/:id", GamesController.edit)
   .delete("/game/:id", GamesController.delete)


module.exports = router;