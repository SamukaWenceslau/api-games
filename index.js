const express = require('express');
const bodyParser = require('body-parser');
const cors = require("cors");


const gamesRoutes= require('./routes/games');


const connection = require('./config/config');


// Express

const app = express();


app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Rotas 

app.use('/',gamesRoutes);

// Conexão com o banco

const connectionDB = async () => {
    try {
    
      await connection.authenticate();
      console.log('Connection has been established successfully.');
    
    } catch (error) {
    
      console.error('Unable to connect to the database:', error);
    
    }
}

connectionDB();

// Config porta

app.listen(3000, () => {
    console.log('API rodando...');
})