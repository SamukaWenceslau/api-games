const { Sequelize } = require('sequelize');

const connection = new Sequelize(
    'nomedobanco', 
    'usuariodobanco', 
    'senhadobanco', 
    {
        host: 'localhost',
        dialect: 'mysql',
        timezone: '-03:00'
    }
);

module.exports = connection;