const Games = require('../models/Games');

module.exports = {

    async index(req, res) {
        try {
            const games = await Games.findAll();
    
            res.statusCode = 200;
            res.json(games);
            
        } catch (error) {
            console.log(error);
        }
    },

    async show (req, res) {
        try {
            if (isNaN(req.params.id)) {
                res.sendStatus(400);
            } else {
        
                const id = parseInt(req.params.id);
        
                const game = await Games.findOne({ where: { id }});
        
                if (game !== null) {

                    res.statusCode = 200;
                    res.json(game);
                    
                } else {
                    res.sendStatus(404);
                }
            }
            
        } catch (error) {
            console.log(error)
        }
    },

    async create (req, res) {
        try {
            const { title, year, price } = req.body;

            await Games.create({ title, year, price });

            res.sendStatus(200);
        } catch (error) {
            console.log(error)
        }
    },

    async edit (req, res) {
        try {

            if (isNaN(req.params.id)) {
                res.sendStatus(400);
        
            } else {
        
                const id = parseInt(req.params.id);
        
                const game = await Games.findOne({ where: { id }});
        
                if (game !== null) {
        
                    const { title, price, year } = req.body;
        
                    if (title !== undefined) {
                        await Games.update({ title }, {where: { id }});
                    }
        
                    if (price !== undefined) {
                        await Games.update({ price }, {where: { id }});
                    }
        
                    if (year !== undefined) {
                        await Games.update({ year }, {where: { id }});
                    }
        
                    res.sendStatus(200);
        
                } else {
                    res.sendStatus(404);
                }
            }
            
        } catch (error) {
            console.log(error);
        }
    },

    async delete (req, res) {
        try {

            if (isNaN(req.params.id)) {
                res.sendStatus(400);
        
            } else {
        
                const id = parseInt(req.params.id);
        
                const game = await Games.findOne({ where: { id }});
        
                if (game !== null) {
        
                    await Games.destroy({ where: { id }});

                    res.sendStatus(200);
        
                } else {
                    res.sendStatus(404);
                }
            }
            
        } catch (error) {
            console.log(error);
        }
    }


}
